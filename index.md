---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Julia 1.8.5
  language: julia
  name: julia-1.8
---

# A note on the standard diffusion curve of TAP analysis

Toby Isaac <[tisaac@anl.gov](mailto:tisaac@anl.gov)>

```{code-cell}
:tags: [remove-cell]
using Plots; plotlyjs();
default(w=2
  , legend=false
  , fontfamily="Georgia"
  , foreground_color="#3E4349"
  , plot_titlefontsize=14
  , titlefontsize=12
  , xlabel=Dict("text"=>"t", "standoff"=> 0)
  );
```

In TAP reactor analysis, the _standard diffusion curve_ ([Gleaves et al.,
1997](https://doi.org/10.1016/S0926-860X(97)00124-5)) describes the outlet flux
intensity of an inert gas that transports through a uniform 1D reactor by
Knudsen diffusion after an instantaneous pulse at the reactor's inlet. If
$C(x,t)$ is the solution of the initial boundary value PDE,

$$
\begin{aligned}
  \partial_t C &= \partial_x^2 C, &x\in(0,1), t > 0,& &\text{(only diffusion in the reactor)},\\
  \partial_x C &= 0, &x=0, t > 0,& &\text{(no flux at inlet after the pulse)},\\
  C &= 0, &x = 1, t > 0,& &\text{(vacuum at outlet)},\\
  C &= \delta(x), &t = 0,& &\text{(instantaneous pulse at inlet)},
\end{aligned}
$$

then the standard diffusion curve is

$$s(t) := - \partial_x C(x,t)|_{x=1}.$$

Using a Fourier series expansion of $\delta(x)$ one can show that, for $t > 0$,

```{math}
:label: sdc-direct
s(t) = \pi \sum_{n=0}^\infty (-1)^n (2n + 1) \exp( - (n + 1/2)^2 \pi^2 t),
```

which is the form of $s$ that appears frequently in publications (for example:
[Yablonsky et al., 2003](https://doi.org/10.1016/S0021-9517(02)00109-4),
[Zheng, 2009](https://www.proquest.com/docview/305021879), [Kunz et al.,
2020](https://doi.org/10.1016/j.cej.2020.125985)).

For each fixed $t$ the series converges absolutely, and for $t > 0.1$ it is
observed that only two terms from the sum are required for an approximation
with at most $2.5\%$ error ([Phanawadee,
1997](https://www.proquest.com/docview/304414726)).

But what if you want to compute $s(t)$ for small values of $t$ close to zero?
This isn't often necessary when comparing $s$ to experimental data, but is
useful when verifying numerical TAP simulation software ([Yonge et al.,
2021](https://doi.org/10.1016/j.cej.2021.129377)).  Directly using a partial
sum of {eq}`sdc-direct` is bad for two related reasons:

1. In exact arithmetic, the number of terms required to approximate $s$ to
   a fixed relative accuracy, $|s(t) - \hat{s}(t)| / |s(t)| < \epsilon$, is
   inversely proportional to $t$ (<a href="#fig-one">fig. 1(a)</a>).

2. The relative error of a series $\sum_n s_n$ computed using floating point
   arithmetic grows like the condition number of the sequence, $\sum_n |s_n|
   / | \sum_n s_n|$.  For the standard diffusion curve, this quantity grows
   extremely quickly as $t \to 0$ (<a href="#fig-one">fig. 1(b)</a>).  In
   double precision arithmetic, the computed value will have no digits of
   accuracy for $t < 0.006$ (<a href="#fig-one">fig. 1(c,d)</a>), and the
   computed value may even have the wrong sign.

We demonstrate these shortcomings with the approximation $\hat{s}$ computed in
different floating point systems, using as many terms of the infinite sum as
are necessary for the floating point value to stabilize.


```{code-cell}
:tags: [hide-cell]
## Compute the standard diffusion curve by adding terms until the floating
#  point value doesn't change.
#
# - set abs=true to compute ∑|sₙ|
# - set ∞=k for only k terms
function sdc_direct(t::T; abs=false, ∞=typemax(Int64))::Tuple{T,Int64} where T <: AbstractFloat
    τ = T(π)^2 * t / 4
    s = zero(t)
    s_old = copy(s)
    sign = abs ? 1 : -1
    for n in 0:∞
        s_old = s
        s += sign^n * (2n + 1) * exp(-(2n + 1)^2 * τ)
        if (s_old == s)
            return (s * π, n)
        end
    end
    return (s * π, ∞ + 1)
end;
```

As a stand-in for the true value of $s$ we will use the same algorithm but with
julia's `BigFloat` (an interface for [GNU MPFR](https://www.mpfr.org/)) with
$2^{-256}$ precision arithmetic, capable of ~77 digits of relative accuracy.

<div id="fig-one">

```{code-cell}
---
tags: [remove-input]
---
T = exp.(-4.5:0.05:4.5) / π
q = plot(title = "(a) terms used to compute ŝ"
    , scale=:log10
    )
p = plot(title = "(b) condition number, ∑|sₙ| / |∑ sₙ|"
    , scale=:log10
    )
r = plot(title = "(c) relative error, |ŝ - s| / |s|"
    , scale=:log10
    )
u = plot(title = "(d) ŝ(t) (log scale)"
    , scale=:log10
    )
first(x) = x[1]
second(x) = x[2]
s_abs = first.(sdc_direct.(BigFloat.(T); abs=true))
types = [BigFloat, Float64, Float32]
sc = [sdc_direct.(Type.(T)) for Type in types]
s = map(a -> first.(a), sc)
c = map(a -> max.(second.(a), 1), sc)

plot!(p, T, s_abs ./ s[1], label=false)
plot!(q, T, [c[1] c[2] c[3]], label=["BigFloat(256)" "Float64" "Float32"])
rel_err(ŝ, s) = abs((ŝ - s) / s)
plot!(r, T, rel_err.([s[2] s[3]], s[1]), label=["Float64" "Float32"])
plot!(u, T, abs.([s[1] s[2] s[3]]), label=["BigFloat(256)" "Float64" "Float32"])
plot(q, p, r, u, layout=(2,2), size=(800, 800), plot_title="Figure 1. Using direct summation", legend=false)
```

</div>

To solve this problem, we use a remarkable functional equation satisfied by the standard diffusion curve,

```{math}
:label: sdc-indirect
(\pi t)^{3/2} s(t) = s((\pi^2 t)^{-1}),
```

which can be proved using the [Poisson summation
formula](https://en.wikipedia.org/wiki/Poisson_summation_formula) and various
[Fourier transform
identities](https://en.wikipedia.org/wiki/Fourier_transform#Tables_of_important_Fourier_transforms).
This means that to evaluate $s(t)$ for $t < \pi^{-1}$ (where direct summation
is unstable), we can evaluate the summation $s(\hat{t})$ for $\hat{t} = (\pi^2
t)^{-1} > \pi^{-1}$ (where direct summation is stable).

```{code-cell}
:tags: [hide-cell]
## Compute the standard diffusion curve by eq. (2)
function sdc(t::T; ∞=typemax(Int64))::Tuple{T,Int64} where T <: AbstractFloat
    t̂ = 1 / (T(π)^2 * t)
    if isinf(t̂)
        return (zero(t), 0)
    end
    if t > t̂
        return sdc_direct(t, ∞=∞)
    else
        s_prime = sdc_direct(t̂, ∞=∞)
        return (s_prime[1] / (t * T(π))^(3/2), s_prime[2])
    end
end;
```

Using this approach:

- In exact arithmetic, only the first term of the sum is required to
  approximate $s(t)$ to $<0.6\%$ relative error for all $t$, and only two terms
  are required for $< 4\cdot10^{-6}\%$ relative error for all $t$ (<a href="#fig-two">fig. 2(a)</a>).
- In floating point arithmetic, at most four terms are necessary for the sum to
  converge in double precision <a href="#fig-two">fig. 2(b)</a>).

<div id="fig-two">

```{code-cell}
---
tags: [remove-input]
---
T = exp.(-4.5:0.05:4.5) / π
p = plot(title = "(a) relative error with k terms"
    , scale=:log10
    , ylim=(1.e-8, 1.0)
    )
q = plot(title = "(b) terms used to compute ŝ"
    , xscale=:log10
    )
r = plot(title = "(c) relative error, |ŝ - s| / |s|"
    , scale=:log10
    )
u = plot(title = "(d) ŝ(t) (log scale)"
    , scale=:log10
    , legend=:bottomright
    )
first(x) = x[1]
second(x) = x[2]
types = [BigFloat, Float64, Float32]
s_true = first.(sdc_direct.(BigFloat.(T)))
s_1 = first.(sdc.(BigFloat.(T); ∞=0))
s_2 = first.(sdc.(BigFloat.(T); ∞=1))
sc = [sdc.(Type.(T)) for Type in types]
s = map(a -> first.(a), sc)
c = map(a -> max.(second.(a), 1), sc)
rel_err(ŝ, s) = abs((ŝ - s) / s)
e_1 = rel_err.(s_1, s[1])
e_2 = rel_err.(s_2, s[1])

plot!(p, T, max.([e_1 e_2], eps(BigFloat(1.0))), label=["1 term" "2 terms"])
#hline!(p, [1 / eps()], line=(:black,:dash))
plot!(q, T, [c[1] c[2] c[3]], label=["BigFloat(256)" "Float64" "Float32"])
plot!(r, Float64[], Float64[], label=false)
plot!(r, T, rel_err.([s[2] s[3]], s_true), label=["Float64" "Float32"])
#hline!(r, [eps()], line=(:black, :dash))
plot!(u, T, abs.([s[1] s[2] s[3]]), label=["BigFloat(256)" "Float64" "Float32"])
#hline!(s, [eps()], line=(:black, :dash))
plot(p, q, r, u, layout=(2,2), size=(800, 800), plot_title="Figure 2. Using the functional equation")
```

</div>
